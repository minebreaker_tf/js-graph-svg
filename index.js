const nodes = [
    { x: 1, y: 1, name: "駅A" },
    { x: 2, y: 1, name: "駅B" },
    { x: 3, y: 1, name: "駅C" },
    { x: 1, y: 2, name: "駅D" },
    { x: 2, y: 2, name: "駅E" },
    { x: 3, y: 2, name: "駅F" },
]

nodes[0].connected = [nodes[1], nodes[3]]
nodes[1].connected = [nodes[0], nodes[2], nodes[4]]
nodes[2].connected = [nodes[1]]
nodes[3].connected = [nodes[0], nodes[4]]
nodes[4].connected = [nodes[1], nodes[3], nodes[5]]
nodes[5].connected = [nodes[4]]

const svgEl = document.getElementById("svg")
const svgNs = "http://www.w3.org/2000/svg"

function search(from, target) {

    function search(searched, nodePairs) {

        const found = nodePairs.find(e => e.node === target)
        if (found) {
            return [...found.ancestors, found.node]
        }

        const notSearchedNodePairs = nodePairs.filter(e => !searched.includes(e.node))

        return search(
            [...searched, ...(notSearchedNodePairs.map(e => e.node))],
            notSearchedNodePairs.flatMap(e => e.node.connected.map(c => ({
                node: c,
                ancestors: [...e.ancestors, e.node]
            })))
        )
    }

    return search([], [{ node: from, ancestors: [] }])
}

// console.log(search(nodes[2], nodes[5]))
// console.log(search(nodes[1], nodes[5]))
// console.log(search(nodes[3], nodes[2]))

const grid = 100

function createDom(node) {
    const x = node.x * grid
    const y = node.y * grid

    const nameEl = document.createElementNS(svgNs, "text")
    nameEl.setAttribute("x", x + 4)
    nameEl.setAttribute("y", y - 18)
    nameEl.setAttribute("font-family", "Meiryo")
    nameEl.setAttribute("font-size", 14)
    nameEl.setAttribute("cursor", "pointer")
    nameEl.innerHTML = node.name
    svgEl.appendChild(nameEl)

    const pointEl = document.createElementNS(svgNs, "circle")
    pointEl.setAttribute("cx", x)
    pointEl.setAttribute("cy", y)
    pointEl.setAttribute("r", 16)
    pointEl.setAttribute("cursor", "pointer")
    svgEl.appendChild(pointEl)

    node.connected.forEach(c => {  // FIXME remove duplication
        const connectionEl = document.createElementNS(svgNs, "line")
        connectionEl.setAttribute("x1", x)
        connectionEl.setAttribute("y1", y)
        connectionEl.setAttribute("x2", c.x * grid)
        connectionEl.setAttribute("y2", c.y * grid)
        connectionEl.setAttribute("stroke", "lightblue")
        connectionEl.setAttribute("stroke-width", 8)
        connectionEl.setAttribute("cursor", "pointer")
        svgEl.insertBefore(connectionEl, svgEl.firstChild)
    })
}

nodes.forEach(e => {
    createDom(e)
})
